/* Copyright (C) 2017 MaterialJ Project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.trangle.materialj.view;

import xyz.trangle.materialj.helper.MaterialColor;
import xyz.trangle.materialj.widget.FrameLayout;
import xyz.trangle.materialj.widget.LinearLayout;

/**
 * Created by Trangle on 2017/7/13.
 */
public class RootView extends FrameLayout {

    //********** Fields, Getters & Setters **********

    private boolean mIsRootView = true;

//    private LinearLayout overLayer;
//    private LinearLayout content;

    //********** Constructor(s) **********

    public RootView(Window window) {
        super(window);
        setBackground(MaterialColor.GREY_50);
//        overLayer = new LinearLayout(window);
    }

}
