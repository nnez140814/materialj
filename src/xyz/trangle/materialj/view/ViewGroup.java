// Copyright (C) 2017 MaterialJ Project
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package xyz.trangle.materialj.view;

import xyz.trangle.materialj.tool.Log;

/**
 * Created by Trangle on 17-7-15.
 */
public class ViewGroup extends View {

    //********** Fields **********

    private static final String TAG = "ViewGroup";

    private final int ARRAY_CAPACITY_INCREMENT = 4;

    /**
     * Views belong to this ViewGroup
     */
    private View[] mChildren;
    private int mChildrenCount;

    //********** Constructor(s) **********

    public ViewGroup(Window window) {
        super(window);
        mChildren = new View[ARRAY_CAPACITY_INCREMENT];
        mChildrenCount = 0;
    }

    //********** Methods **********

    /**
     * Add a view to the group
     * @param child Child view
     */
    public void addView(View child) {
        addView(child, -1);
    }

    /**
     * Add a view to the group with index
     * @param child  Child view
     * @param index Index that the child would be inserted
     */
    public void addView(View child, int index) {
        if(child.mParent != null) {
            Log.e(TAG, "Trying to attach a view which already has a parent to another ViewGroup!");
            throw new IllegalStateException("Cannot attach a view which already has a parent!");
        }
        View[] children = mChildren;
        final int count = mChildrenCount;
        final int size = mChildren.length;
        if(index == mChildrenCount) {
            if(size == count) {
                mChildren = new View[size + ARRAY_CAPACITY_INCREMENT];
                System.arraycopy(children, 0, mChildren, 0, size);
                children = mChildren;
            }
            children[mChildrenCount++] = child;
        }else if(index < count) {
            if(size == count) {
                mChildren = new View[size + ARRAY_CAPACITY_INCREMENT];
                System.arraycopy(children, 0, mChildren, 0, index);
                System.arraycopy(children, index, mChildren, index + 1, count - index);
                children = mChildren;
            }else{
                System.arraycopy(children, index, children, index + 1, count - index);
            }
            children[index] = child;
            mChildrenCount++;
        }else{
            throw new IndexOutOfBoundsException("index=" + index + " count=" + count);
        }
        child.mParent = this;
    }

//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//
//    }

    /**
     * Ask children to measure themselves
     * @param child                   Child view
     * @param parentWidthMeasureSpec  parent width MeasureSpec
     * @param parentHeightMeasureSpec parent height MeasureSpec
     */
    protected void measureChild(View child, int parentWidthMeasureSpec, int parentHeightMeasureSpec) {
        final LayoutParams layoutParams = child.getLayoutParam();

        final int childWidthMeasureSpec = getChildMeasureSpec(parentWidthMeasureSpec,
                mPaddingLeft + mPaddingRight, layoutParams.getWidth());
        final int childHeightMeasureSpec = getChildMeasureSpec(parentHeightMeasureSpec,
                mPaddingTop + mPaddingBottom, layoutParams.getHeight());

        child.measure(childWidthMeasureSpec, childHeightMeasureSpec);
    }

    protected int getChildMeasureSpec(int spec, int padding, int childDimension) {
        int specMode = MeasureSpec.getMode(spec);
        int specSize = MeasureSpec.getSize(spec);

        int size = Math.max(0, specSize - padding);

        int resultSize = 0;
        int resultMode = 0;
        switch (specMode) {
            // Parent has imposed an exact size on us
            case MeasureSpec.EXACTLY:
                if (childDimension >= 0) {
                    resultSize = childDimension;
                    resultMode = MeasureSpec.EXACTLY;
                } else if (childDimension == LayoutParams.MATCH_PARENT) {
                    // Child wants to be our size. So be it.
                    resultSize = size;
                    resultMode = MeasureSpec.EXACTLY;
                } else if (childDimension == LayoutParams.WRAP_CONTENT) {
                    // Child wants to determine its own size. It can't be
                    // bigger than us.
                    resultSize = size;
                    resultMode = MeasureSpec.AT_MOST;
                }
                break;

            // Parent has imposed a maximum size on us
            case MeasureSpec.AT_MOST:
                if (childDimension >= 0) {
                    // Child wants a specific size... so be it
                    resultSize = childDimension;
                    resultMode = MeasureSpec.EXACTLY;
                } else if (childDimension == LayoutParams.MATCH_PARENT) {
                    // Child wants to be our size, but our size is not fixed.
                    // Constrain child to not be bigger than us.
                    resultSize = size;
                    resultMode = MeasureSpec.AT_MOST;
                } else if (childDimension == LayoutParams.WRAP_CONTENT) {
                    // Child wants to determine its own size. It can't be
                    // bigger than us.
                    resultSize = size;
                    resultMode = MeasureSpec.AT_MOST;
                }
                break;

            // Parent asked to see how big we want to be
            case MeasureSpec.UNSPECIFIED:
                if (childDimension >= 0) {
                    // Child wants a specific size... let him have it
                    resultSize = childDimension;
                    resultMode = MeasureSpec.EXACTLY;
                } else if (childDimension == LayoutParams.MATCH_PARENT) {
                    // Child wants to be our size... find out how big it should
                    // be
                    resultSize = 0;
                    resultMode = MeasureSpec.UNSPECIFIED;
                } else if (childDimension == LayoutParams.WRAP_CONTENT) {
                    // Child wants to determine its own size.... find out how
                    // big it should be
                    resultSize = 0;
                    resultMode = MeasureSpec.UNSPECIFIED;
                }
                break;
        }
        return MeasureSpec.makeMeasureSpec(resultSize, resultMode);
    }
}
