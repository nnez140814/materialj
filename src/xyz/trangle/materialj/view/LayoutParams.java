// Copyright (C) 2017 MaterialJ Project
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package xyz.trangle.materialj.view;

/**
 * Created by Trangle on 17-7-13.
 * <p>
 * Class to store layout params
 */
public class LayoutParams {

    //********** Fields, Getters & Setters **********

    public static final int MATCH_PARENT = -1;

    public static final int WRAP_CONTENT = -2;

    /**
     * Width
     */
    private int mWidth = WRAP_CONTENT;

    public void setWidth(int layoutWidth) {
        this.mWidth = layoutWidth;
    }

    public int getWidth() {
        return mWidth;
    }

    /**
     * Height
     */
    private int mHeight = WRAP_CONTENT;

    public void setHeight(int mHeight) {
        this.mHeight = mHeight;
    }

    public int getHeight() {
        return mHeight;
    }
}
