/* Copyright (C) 2017 MaterialJ Project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.trangle.materialj.view;

import xyz.trangle.materialj.graphics.Canvas;
import xyz.trangle.materialj.helper.MaterialColor;

import java.awt.*;

/**
 * Created by Trangle on 2017/7/12.
 * <p>
 * Basic interface component.
 */
public class View {

    //********** Fields **********

    private final String TAG = "View";

    // ----- Public Flags -----
    public final int FLAG_VIEW_VISIBLE = 0b1;

    // ----- Private Flags -----
    private int mPrivateFlags = 0;
    private final int PFLAG_FORCE_LAYOUT = 0b1;
    private final int PFLAG_MEASURED_DIMENSION_SET = 0b10;
    private final int PFLAG_LAYOUT_REQUIRED = 0b100;
    private final int PFLAG_MEASURE_BEFORE_LAYOUT = 0b1000;
    private final int PFLAG_INVALIDATED = 0b10000;

    // ----- Masks -----
    public static final int MEASURED_SIZE_MASK = 0x00ffffff;

    private boolean mIsRootView = false;

    private boolean mDrawLayoutBounds = false;


    /**
     * Parent ViewGroup of this view
     */
    protected ViewGroup mParent;

    protected int mMeasuredWidth, mMeasuredHeight;
    protected int mTop, mRight, mBottom, mLeft;
    protected int mPaddingTop, mPaddingRight, mPaddingBottom, mPaddingLeft;

    private int mOldWidthMeasureSpec, mOldHeightMeasureSpec;

    /**
     * Window contains this view
     */
    private Window mWindow;

    /**
     * Layout param
     */
    private LayoutParams mLayoutParams;

    public LayoutParams getLayoutParam() {
        return mLayoutParams;
    }

    /**
     * Get measured width
     *
     * @return The raw width of this view
     */
    public final int getMeasuredWidth() {
        return mMeasuredWidth & MEASURED_SIZE_MASK;
    }

    /**
     * Get measured height
     *
     * @return The raw height of this view
     */
    public final int getMeasuredHeight() {
        return mMeasuredHeight & MEASURED_SIZE_MASK;
    }

    /**
     * Background color
     */
    private Color mBackgroundColor;

    public void setBackground(int rgb) {
        mBackgroundColor = new Color(rgb);
    }

    //********** Constructor **********

    public View(Window window) {
        mWindow = window;
        mLayoutParams = new LayoutParams();
        mDrawLayoutBounds = window.getContext().isDrawLayoutBoundsEnabled();
    }

    //********** Methods **********

    /**
     * Set padding
     *
     * @param t Top padding
     * @param r Right padding
     * @param b Bottom padding
     * @param l Left padding
     */
    public void setPadding(int t, int r, int b, int l) {
        mPaddingTop = t;
        mPaddingRight = r;
        mPaddingBottom = b;
        mPaddingLeft = l;
    }

    /**
     * Measure this view.
     *
     * @param widthMeasureSpec  Horizontal space requirements as imposed by the
     *                          parent
     * @param heightMeasureSpec Vertical space requirements as imposed by the
     *                          parent
     */
    public final void measure(int widthMeasureSpec, int heightMeasureSpec) {
        final boolean forceLayout = (mPrivateFlags & PFLAG_FORCE_LAYOUT) == PFLAG_FORCE_LAYOUT;
        final boolean isExactly = MeasureSpec.getMode(widthMeasureSpec) == MeasureSpec.EXACTLY &&
                MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.EXACTLY;
        final boolean matchingSize = isExactly &&
                getMeasuredWidth() == MeasureSpec.getSize(widthMeasureSpec) &&
                getMeasuredHeight() == MeasureSpec.getSize(heightMeasureSpec);
        if(forceLayout || !matchingSize &&
                (widthMeasureSpec != mOldWidthMeasureSpec ||
                        heightMeasureSpec != mOldHeightMeasureSpec)) {
            mPrivateFlags &= ~PFLAG_MEASURED_DIMENSION_SET;

            // TODO: Consider to add cache someday.
            onMeasure(widthMeasureSpec, heightMeasureSpec);

            if((mPrivateFlags & PFLAG_MEASURED_DIMENSION_SET) != PFLAG_MEASURED_DIMENSION_SET) {
                throw new IllegalStateException("onMeasure() didn't set the measured dimension by calling setMeasuredDimension()");
            }

            mOldWidthMeasureSpec = widthMeasureSpec;
            mOldHeightMeasureSpec = heightMeasureSpec;
        }
    }

    /**
     * Measure the view (and its children) to determine the measured width & height.
     * This method should be overridden by subclasses to provide efficient measuring
     * algorithm.
     * <p>
     * Subclasses should't call super.onMeasure() if overridden
     *
     * @param widthMeasureSpec  Horizontal space requirements as imposed by the
     *                          parent
     * @param heightMeasureSpec Vertical space requirements as imposed by the
     *                          parent
     */
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(getDefaultSize(8, widthMeasureSpec), getDefaultSize(8, heightMeasureSpec));
    }

    protected final void setMeasuredDimension(int measuredWidth, int measuredHeight) {
        mMeasuredWidth = measuredWidth;
        mMeasuredHeight = measuredHeight;

        mPrivateFlags |= PFLAG_MEASURED_DIMENSION_SET;
    }

    public static int getDefaultSize(int size, int measureSpec) {
        int result = size;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        switch(specMode) {
            case MeasureSpec.UNSPECIFIED:
                result = size;
                break;
            case MeasureSpec.AT_MOST:
            case MeasureSpec.EXACTLY:
                result = specSize;
                break;
        }
        return result;
    }

    /**
     * Assign the size and position to the view and all of its children.
     *
     * @param top    Top position, relative to parent.
     * @param right  Right position, relative to parent.
     * @param bottom Bottom position, relative to parent.
     * @param left   Left position, relative to parent.
     */
    public final void layout(int top, int right, int bottom, int left) {
        /*int oldTop = mTop;
        int oldRight = mRight;
        int oldBottom = mBottom;
        int oldLeft = mLeft;*/

        boolean changed = setFrame(top, right, bottom, left);

        if(changed || (mPrivateFlags & PFLAG_LAYOUT_REQUIRED) == PFLAG_LAYOUT_REQUIRED) {
            onLayout(changed, top, right, bottom, left);
            mPrivateFlags &= ~PFLAG_LAYOUT_REQUIRED;
        }
    }

    protected void onLayout(boolean changed, int top, int right, int bottom, int left) {

    }

    protected boolean setFrame(int top, int right, int bottom, int left) {
        if(mTop == top && mRight == right && mBottom == bottom && mLeft == left) {
            return false;
        }else{
            int oldWidth = mRight - mLeft;
            int oldHeight = mBottom - mLeft;
            int newWidth = right - left;
            int newHeight = bottom - top;

            boolean sizeChanged = (newWidth != oldWidth) || (newHeight != oldHeight);

            // invalidate(sizeChanged); TODO: invalidate(boolean)

            mTop = top;
            mRight = right;
            mBottom = bottom;
            mLeft = left;

            return true;
        }
    }

    /**
     * Force this view to layout
     * @param measureBeforeLayout Needs measure before layout
     */
    public void forceLayout(boolean measureBeforeLayout) {
        mPrivateFlags |= PFLAG_FORCE_LAYOUT;
        mPrivateFlags |= PFLAG_INVALIDATED;
        if(measureBeforeLayout) {
            mPrivateFlags |= PFLAG_MEASURE_BEFORE_LAYOUT;
        }
    }

    /**
     * Basic draw function.
     * Custom view should use onDraw(Canvas canvas) instead.
     *
     * @param canvas The canvas to draw the view on.
     */
    public final void draw(Canvas canvas) {
        /*
         * Steps:
         *      1. Draw the background.
         *      2. Draw the content.
         *      3. Draw children
         *      4. Draw foreground.
         *      5. Draw layout bounds if needed. (Debug usage)
         */

        // Step1: draw background
        drawBackground(canvas);
        // Step2: draw content
        onDraw(canvas);
        // Step3: draw children
        dispatchDraw(canvas);
        // Step4: draw foreground
        drawForeground(canvas);
        // Stop5: draw layout bounds if needed
        if(mDrawLayoutBounds) {
            drawLayoutBounds(canvas);
        }
    }

    /**
     * Draw background of this view.
     *
     * @param canvas The canvas to draw the view on.
     */
    private void drawBackground(Canvas canvas) {
        // TODO: Finish this method.
        canvas.fillRect(mBackgroundColor, 0, 0, getMeasuredWidth(), getMeasuredHeight());
    }

    /**
     * Custom view should use override this method to draw.
     *
     * @param canvas
     */
    public void onDraw(Canvas canvas) {
    }

    /**
     * Called by draw() to draw children. It may be overridden by sub classes.
     *
     * @param canvas The canvas to draw the view on.
     */
    protected void dispatchDraw(Canvas canvas) {
    }

    /**
     * Draw foreground of this view.
     *
     * @param canvas The canvas to draw the view on.
     */
    private void drawForeground(Canvas canvas) {
        // TODO: Finish this method.
    }

    /**
     * Method to draw the bounds of the view.
     * Will be called if there is --showLayoutBounds in the command line.
     *
     * @param canvas The canvas to draw the view on.
     */
    private void drawLayoutBounds(Canvas canvas) {
        // TODO: Finish this method.
    }

    //********** Inner classes **********

    @SuppressWarnings("NumericOverflow")
    public static class MeasureSpec {
        private static final int MODE_SHIFT = 30;
        private static final int MODE_MASK = 0x3 << MODE_SHIFT;

        public static final int UNSPECIFIED = 0; // 0 << MODE_SHIFT;
        public static final int EXACTLY = 1 << MODE_SHIFT;
        public static final int AT_MOST = 2 << MODE_SHIFT;

        public static int makeMeasureSpec(int size, int mode) {
            return (size & ~MODE_MASK) | (mode & MODE_MASK);
        }

        public static int getMode(int measureSpec) {
            return (measureSpec & MODE_MASK);
        }

        public static int getSize(int measureSpec) {
            return (measureSpec & ~MODE_MASK);
        }
    }
}
