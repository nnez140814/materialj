/* Copyright (C) 2017 MaterialJ Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.trangle.materialj.view;

import xyz.trangle.materialj.content.Context;
import xyz.trangle.materialj.graphics.Canvas;
import xyz.trangle.materialj.listener.OnFramePaintListener;
import xyz.trangle.materialj.tool.Log;

import java.awt.*;
import java.awt.event.*;

/**
 * Created by Trangle on 2017/7/12.
 * <p>
 * Class for window.
 */

public class Window implements WindowListener, OnFramePaintListener, WindowFocusListener, WindowStateListener, ComponentListener, KeyListener {

    //********** Fields, Getters & Setters **********

    private static final String TAG = "Window";

    public static final int WINDOW_ID_BASE = 0;

    private static final int DEFAULT_WIDTH = 800;
    private static final int DEFAULT_HEIGHT = 600;

    private static int sNextWindowID = WINDOW_ID_BASE;

    // ----- Private Flags -----
    private int mPrivateFlags = 0;
    /**
     * Flag indicates if a measurement should be performed.
     */
    private final int PFLAG_MEASURE_NEEDED = 0b1;
    private final int PFLAG_LAYOUT_NEEDED = 0b10;

    /**
     * Latest window width
     */
    private int mWidth = -1;

    /**
     * Latest window height
     */
    private int mHeight = -1;

    private Context mContext;

    public Context getContext() {
        return mContext;
    }

    /**
     * Visibility of this window
     */
    private boolean mVisible = false;

    public void setVisible(boolean visible) {
        this.mVisible = visible;
        mWindow.setVisible(visible);
        if(visible) {
            mLooper.start();
        }
    }

    /**
     * Identification of a window.
     */
    private int mWindowId;

    public int getId() {
        return mWindowId;
    }

    /**
     * Window itself.
     */
    private BasicFrame mWindow;

    /**
     * Title of a window
     */
    private String mTitle = "";

    public void setTitle(String title) {
        this.mTitle = title;
        mWindow.setTitle(title);
    }

    public String getTitle() {
        return mTitle;
    }

    /**
     * Canvas of this window
     */
    private Canvas mCanvas;

    /**
     * Root view of this window
     */
    private RootView mRootView;

    /**
     * Frame rate
     */
    private int mFrameRate = 60;
    private int mFrameInterval = 10;

    public void setFrameRate(int frameRate) {
        mFrameRate = frameRate;
        mFrameInterval = 1000 / frameRate;
    }

    public int getFrameRate() {
        return mFrameRate;
    }

    /**
     * The looper
     */
    private Thread mLooper = new Thread(() -> {
        Log.d(TAG, "Looper start!");
        while(mVisible) {
//            mWindow.repaint();
            try {
                Thread.sleep(mFrameInterval);
            }catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
        Log.d(TAG, "Looper quit!");
    });

    //********** Constructors **********

    public Window(Context context) {
        this.mContext = context;
        makeWindow();
        setWindowSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        mCanvas = new Canvas();
        mRootView = new RootView(this);
    }

    public Window(Context context, String title) {
        this(context);
        setTitle(title);
    }

    //********** Methods **********

    /**
     * Method to make a window
     */
    private void makeWindow() {
        mWindowId = sNextWindowID;
        sNextWindowID++;
        mWindow = new BasicFrame();
        mWindow.setUndecorated(true);
        mWindow.setOnFramePaintListener(this);
        mWindow.addWindowListener(this);
        mWindow.addWindowFocusListener(this);
        mWindow.addWindowStateListener(this);
        mWindow.addComponentListener(this);
        mWindow.addKeyListener(this);
    }

    /**
     * Set size of this window
     *
     * @param width  Width of this window
     * @param height Height of this window
     */
    public void setWindowSize(int width, int height) {
        mWindow.setSize(width, height);
    }

    /**
     * Measure, Layout and Draw the root view.
     */
    private void performTraversals() {
        // **** Measure ****
        if(mWidth != mWindow.getWidth() || mHeight != mWindow.getHeight()) {
            mPrivateFlags |= PFLAG_MEASURE_NEEDED;
            mPrivateFlags |= PFLAG_LAYOUT_NEEDED;

            mWidth = mWindow.getWidth();
            mHeight = mWindow.getHeight();
        }
        if((mPrivateFlags & PFLAG_MEASURE_NEEDED) == PFLAG_MEASURE_NEEDED) {
            int rootWidthMeasureSpec = View.MeasureSpec.makeMeasureSpec(mWindow.getWidth(), View.MeasureSpec.EXACTLY);
            int rootHeightMeasureSpec = View.MeasureSpec.makeMeasureSpec(mWindow.getHeight(), View.MeasureSpec.EXACTLY);
            mRootView.measure(rootWidthMeasureSpec, rootHeightMeasureSpec);

            mPrivateFlags &= ~PFLAG_MEASURE_NEEDED;
        }

        // **** Layout ****
        if((mPrivateFlags & PFLAG_LAYOUT_NEEDED) == PFLAG_LAYOUT_NEEDED) {
            mRootView.forceLayout(true);
            mRootView.layout(0, 0, 0, 0);

            mPrivateFlags &= ~PFLAG_LAYOUT_NEEDED;
        }

        // **** Draw ****
        mRootView.draw(mCanvas);
    }

    //********** Overrides **********

    @Override
    public void onFramePaint(Graphics2D g2) {
        g2.setRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));
        mCanvas.setGraphics(g2);
        performTraversals();
//        mCanvas.translate(20, 50);
//        mCanvas.drawLine(0xABCDEF, 0, 0, 100, 200);
    }

    @Override
    public void windowOpened(WindowEvent e) {
        Log.v(TAG, "windowOpened");
    }

    @Override
    public void windowClosing(WindowEvent e) {
        // TODO Call window closing listener here
        mWindow.dispose();
        Log.v(TAG, "windowClosing");
    }

    @Override
    public void windowClosed(WindowEvent e) {
        mVisible = false;
        Log.v(TAG, "windowClosed");
    }

    @Override
    public void windowIconified(WindowEvent e) {
        Log.v(TAG, "windowIconified");
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        Log.v(TAG, "windowDeiconified");
    }

    @Override
    public void windowActivated(WindowEvent e) {
        Log.v(TAG, "windowActivated");
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        Log.v(TAG, "windowDeactivated");
    }

    @Override
    public void windowGainedFocus(WindowEvent e) {
        Log.v(TAG, "windowGainedFocus");
    }

    @Override
    public void windowLostFocus(WindowEvent e) {
        Log.v(TAG, "windowLostFocus");
    }

    @Override
    public void windowStateChanged(WindowEvent e) {
        Log.v(TAG, "windowStateChanged");
    }

    @Override
    public void componentResized(ComponentEvent e) {
        Log.v(TAG, "componentResized");
    }

    @Override
    public void componentMoved(ComponentEvent e) {
        Log.v(TAG, "componentMoved");
    }

    @Override
    public void componentShown(ComponentEvent e) {
        Log.v(TAG, "componentShown");
    }

    @Override
    public void componentHidden(ComponentEvent e) {
        Log.v(TAG, "componentHidden");
    }

    @Override
    public void keyTyped(KeyEvent e) {
        Log.v(TAG, "keyTyped");
        if(e.getKeyChar() == 27) {
            setVisible(false);
            try {
                Thread.sleep(2000);
            }catch(InterruptedException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }
        Log.v(TAG, String.valueOf((int) e.getKeyChar()));
    }

    @Override
    public void keyPressed(KeyEvent e) {
        Log.v(TAG, "keyPressed");
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Log.v(TAG, "keyReleased");
    }

    private class BasicFrame extends Frame {
        private OnFramePaintListener onFramePaintListener;

        public void setOnFramePaintListener(OnFramePaintListener onFramePaintListener) {
            this.onFramePaintListener = onFramePaintListener;
        }

        @Override
        public void paint(Graphics g) {
            onFramePaintListener.onFramePaint((Graphics2D) g);
            super.paint(g);
        }
    }

}
