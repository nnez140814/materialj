/* Copyright (C) 2017 MaterialJ Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.trangle.materialj.app;

import xyz.trangle.materialj.content.Context;
import xyz.trangle.materialj.tool.Log;
import xyz.trangle.materialj.view.Window;

import java.util.ArrayList;

/**
 * Created by Trangle on 17-7-12.
 * <p>
 * Main Entry of Applications.
 * <p>
 * You may start your app with the code below:
 * <p>
 * public static void main(String[] args) {
 * Application app = new Application();
 * }
 */

public class Application extends Context {

    //********** Fields **********

    private static final String TAG = "Application";

    private ArrayList<Window> mWindowList = new ArrayList<>();

    //********** Constructor(s) **********

    public Application(String[] args) {
        int size = args.length;
        for(int i = 0; i < size; i++) {
            switch(args[i]) {
                case "--showLayoutBounds":
                    mDrawLayoutBoundsEnabled = true;
                    Log.d(TAG, "showLayoutBounds is enabled");
                    break;
                case "--logLevel":
                    try {
                        Log.setLogLimitation(args[++i]);
                    }catch(IllegalArgumentException e) {
                        Log.e(TAG, e.getMessage() + "\n\t--logLevel must be one of v, d, i, w and e!");
                    }
                    break;
                default:
                    Log.d(TAG, "Unknown argument: " + args[i]);
            }
        }
    }

    //********** Methods **********

    /**
     * Create a new window with empty title
     *
     * @return Id of the new window
     */
    public int createWindow() {
        Window window = new Window(this);
        mWindowList.add(window);
        return window.getId();
    }

    /**
     * Create a new window and set title
     *
     * @param title Title of the new window
     * @return Id of the new window
     */
    public int createWindow(String title) {
        Window window = new Window(this, title);
        mWindowList.add(window);
        return window.getId();
    }

    /**
     * Get a window by its id
     *
     * @param id id of a window
     * @return Target window if exist. Null if not exist.
     */
    public Window getWindow(int id) {
        int size = mWindowList.size();
        for(int i = 0; i < size; i++) {
            if(mWindowList.get(i).getId() == id) {
                return mWindowList.get(i);
            }
        }
        return null;
    }
}
