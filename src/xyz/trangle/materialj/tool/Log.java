// Copyright (C) 2017 MaterialJ Project
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package xyz.trangle.materialj.tool;

import java.io.PrintStream;

/**
 * Created by Trangle on 17-7-15.
 * <p>
 * Provides a better way to echo logs.
 */
public class Log {

    //********** Fields, Getters & Setters **********

    /**
     * Priority constants.
     */
    public static final int VERBOSE = 2;
    public static final int DEBUG = 3;
    public static final int INFO = 4;
    public static final int WARN = 5;
    public static final int ERROR = 6;

    private static int mLogLimitation = 2;

    /**
     * Set the minimum log level to be logged.
     *
     * @param type Priority constants.
     */
    public static void setLogLimitation(int type) {
        mLogLimitation = type;
    }

    /**
     * Set the minimum log level to be logged.
     *
     * @param type Priority string.
     */
    public static void setLogLimitation(String type) throws IllegalArgumentException {
        switch(type) {
            case "v":
                setLogLimitation(VERBOSE);
                break;
            case "d":
                setLogLimitation(DEBUG);
                break;
            case "i":
                setLogLimitation(INFO);
                break;
            case "w":
                setLogLimitation(WARN);
                break;
            case "e":
                setLogLimitation(ERROR);
                break;
            default:
                throw new IllegalArgumentException("Unknown log level: " + type);
        }
    }

    //********** Methods **********

    /**
     * Verbose message
     *
     * @param tag Used to identify the source of message.
     * @param msg Message itself.
     */
    public static void v(String tag, String msg) {
        if(mLogLimitation > VERBOSE) return;
        log(System.out, "v", tag, msg);
    }

    /**
     * Debug message
     *
     * @param tag Used to identify the source of message.
     * @param msg Message itself.
     */
    public static void d(String tag, String msg) {
        if(mLogLimitation > DEBUG) return;
        log(System.out, "d", tag, msg);
    }

    /**
     * Info message
     *
     * @param tag Used to identify the source of message.
     * @param msg Message itself.
     */
    public static void i(String tag, String msg) {
        if(mLogLimitation > INFO) return;
        log(System.out, "i", tag, msg);
    }

    /**
     * Warning message
     *
     * @param tag Used to identify the source of message.
     * @param msg Message itself.
     */
    public static void w(String tag, String msg) {
        if(mLogLimitation > WARN) return;
        log(System.out, "w", tag, msg);
    }

    /**
     * Error message
     *
     * @param tag Used to identify the source of message.
     * @param msg Message itself.
     */
    public static void e(String tag, String msg) {
        if(mLogLimitation > ERROR) return;
        log(System.err, "e", tag, msg);
    }

    /**
     * Format message output.
     *
     * @param target Target output stream
     * @param level  Message level
     * @param tag    Used to identify the source of message.
     * @param msg    Message itself.
     */
    private static void log(PrintStream target, String level, String tag, String msg) {
        target.print(level);
        target.print('.');
        target.print(tag);
        target.print('/');
        target.println(msg);
    }

}
