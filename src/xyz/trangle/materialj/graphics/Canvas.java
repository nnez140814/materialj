/* Copyright (C) 2017 MaterialJ Project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.trangle.materialj.graphics;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.util.Stack;

/**
 * Created by Trangle on 2017/7/12.
 */
public class Canvas {

    //********** Fields, Getters & Setters **********

    /**
     * Graphics2D powered by Java2D
     */
    private Graphics2D mGraphics;

    public void setGraphics(Graphics2D g2) {
        this.mGraphics = g2;
    }

    /**
     * Transform stack
     */
    private Stack<AffineTransform> mSavedTransforms = new Stack<>();

    //********** Constructors **********

    public Canvas() {
    }

    //********** Methods **********

    public void saveMatrix() {
        mSavedTransforms.push(mGraphics.getTransform());
        // TODO
    }

    public void restoreMatrix() {
        mGraphics.setTransform(mSavedTransforms.pop());
        // TODO
    }

    /**
     * Translate this matrix
     *
     * @param dx The distance in x
     * @param dy The distance in y
     */
    public void translate(int dx, int dy) {
        mGraphics.translate(dx, dy);
    }

    /**
     * Translate this matrix
     *
     * @param dx The distance in x
     * @param dy The distance in y
     */
    public void translate(double dx, double dy) {
        mGraphics.translate(dx, dy);
    }

    /**
     * Scale this matrix
     *
     * @param sx The scale in x
     * @param sy The scale in y
     */
    public void scale(double sx, double sy) {
        mGraphics.scale(sx, sy);
    }

    public void drawLine(int color, int x1, int y1, int x2, int y2) {
        mGraphics.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
        mGraphics.drawLine(x1, y1, x2, y2);
    }

    public void fillRect(int color, int x, int y, int w, int h) {
        fillRect(new Color(color), x, y, w, h);
    }

    public void fillRect(Color color, int x, int y, int w, int h) {
        mGraphics.setColor(color);
        mGraphics.fillRect(x, y, w, h);
    }
}
