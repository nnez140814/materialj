// Copyright (C) 2017 MaterialJ Project
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package xyz.trangle.materialj.helper;

/**
 * Created by Trangle on 17-7-13.
 *
 * Class which contains colors defined by Material
 * Design color pattern.
 *
 * Users should not instancing this class.
 */
public class MaterialColor {
    public final static int RED_50 = 0xFFEBEE;
    public final static int RED_100 = 0xFFCDD2;
    public final static int RED_200 = 0xEF9A9A;
    public final static int RED_300 = 0xE57373;
    public final static int RED_400 = 0xEF5350;
    public final static int RED_500 = 0xF44336;
    public final static int RED_600 = 0xE53935;
    public final static int RED_700 = 0xD32F2F;
    public final static int RED_800 = 0xC62828;
    public final static int RED_900 = 0xB71C1C;
    public final static int RED_A100 = 0xFF8A80;
    public final static int RED_A200 = 0xFF5252;
    public final static int RED_A400 = 0xFF1744;
    public final static int RED_A700 = 0xD50000;

    public final static int PINK_50 = 0xFCE4EC;
    public final static int PINK_100 = 0xF8BBD0;
    public final static int PINK_200 = 0xF48FB1;
    public final static int PINK_300 = 0xF06292;
    public final static int PINK_400 = 0xEC407A;
    public final static int PINK_500 = 0xE91E63;
    public final static int PINK_600 = 0xD81B60;
    public final static int PINK_700 = 0xC2185B;
    public final static int PINK_800 = 0xAD1457;
    public final static int PINK_900 = 0x880E4F;
    public final static int PINK_A100 = 0xFF80AB;
    public final static int PINK_A200 = 0xFF4081;
    public final static int PINK_A400 = 0xF50057;
    public final static int PINK_A700 = 0xC51162;

    public final static int PURPLE_50 = 0xF3E5F5;
    public final static int PURPLE_100 = 0xE1BEE7;
    public final static int PURPLE_200 = 0xCE93D8;
    public final static int PURPLE_300 = 0xBA68C8;
    public final static int PURPLE_400 = 0xAB47BC;
    public final static int PURPLE_500 = 0x9C27B0;
    public final static int PURPLE_600 = 0x8E24AA;
    public final static int PURPLE_700 = 0x7B1FA2;
    public final static int PURPLE_800 = 0x6A1B9A;
    public final static int PURPLE_900 = 0x4A148C;
    public final static int PURPLE_A100 = 0xEA80FC;
    public final static int PURPLE_A200 = 0xE040FB;
    public final static int PURPLE_A400 = 0xD500F9;
    public final static int PURPLE_A700 = 0xAA00FF;

    public final static int DEEP_PURPLE_50 = 0xEDE7F6;
    public final static int DEEP_PURPLE_100 = 0xD1C4E9;
    public final static int DEEP_PURPLE_200 = 0xB39DDB;
    public final static int DEEP_PURPLE_300 = 0x9575CD;
    public final static int DEEP_PURPLE_400 = 0x7E57C2;
    public final static int DEEP_PURPLE_500 = 0x673AB7;
    public final static int DEEP_PURPLE_600 = 0x5E35B1;
    public final static int DEEP_PURPLE_700 = 0x512DA8;
    public final static int DEEP_PURPLE_800 = 0x4527A0;
    public final static int DEEP_PURPLE_900 = 0x311B92;
    public final static int DEEP_PURPLE_A100 = 0xB388FF;
    public final static int DEEP_PURPLE_A200 = 0x7C4DFF;
    public final static int DEEP_PURPLE_A400 = 0x651FFF;
    public final static int DEEP_PURPLE_A700 = 0x6200EA;

    public final static int INDIGO_50 = 0xE8EAF6;
    public final static int INDIGO_100 = 0xC5CAE9;
    public final static int INDIGO_200 = 0x9FA8DA;
    public final static int INDIGO_300 = 0x7986CB;
    public final static int INDIGO_400 = 0x5C6BC0;
    public final static int INDIGO_500 = 0x3F51B5;
    public final static int INDIGO_600 = 0x3949AB;
    public final static int INDIGO_700 = 0x303F9F;
    public final static int INDIGO_800 = 0x283593;
    public final static int INDIGO_900 = 0x1A237E;
    public final static int INDIGO_A100 = 0x8C9EFF;
    public final static int INDIGO_A200 = 0x536DFE;
    public final static int INDIGO_A400 = 0x3D5AFE;
    public final static int INDIGO_A700 = 0x304FFE;

    public final static int BLUE_50 = 0xE3F2FD;
    public final static int BLUE_100 = 0xBBDEFB;
    public final static int BLUE_200 = 0x90CAF9;
    public final static int BLUE_300 = 0x64B5F6;
    public final static int BLUE_400 = 0x42A5F5;
    public final static int BLUE_500 = 0x2196F3;
    public final static int BLUE_600 = 0x1E88E5;
    public final static int BLUE_700 = 0x1976D2;
    public final static int BLUE_800 = 0x1565C0;
    public final static int BLUE_900 = 0x0D47A1;
    public final static int BLUE_A100 = 0x82B1FF;
    public final static int BLUE_A200 = 0x448AFF;
    public final static int BLUE_A400 = 0x2979FF;
    public final static int BLUE_A700 = 0x2962FF;

    public final static int LIGHT_BLUE_50 = 0xE1F5FE;
    public final static int LIGHT_BLUE_100 = 0xB3E5FC;
    public final static int LIGHT_BLUE_200 = 0x81D4FA;
    public final static int LIGHT_BLUE_300 = 0x4FC3F7;
    public final static int LIGHT_BLUE_400 = 0x29B6F6;
    public final static int LIGHT_BLUE_500 = 0x03A9F4;
    public final static int LIGHT_BLUE_600 = 0x039BE5;
    public final static int LIGHT_BLUE_700 = 0x0288D1;
    public final static int LIGHT_BLUE_800 = 0x0277BD;
    public final static int LIGHT_BLUE_900 = 0x01579B;
    public final static int LIGHT_BLUE_A100 = 0x80D8FF;
    public final static int LIGHT_BLUE_A200 = 0x40C4FF;
    public final static int LIGHT_BLUE_A400 = 0x00B0FF;
    public final static int LIGHT_BLUE_A700 = 0x0091EA;

    public final static int CYAN_50 = 0xE0F7FA;
    public final static int CYAN_100 = 0xB2EBF2;
    public final static int CYAN_200 = 0x80DEEA;
    public final static int CYAN_300 = 0x4DD0E1;
    public final static int CYAN_400 = 0x26C6DA;
    public final static int CYAN_500 = 0x00BCD4;
    public final static int CYAN_600 = 0x00ACC1;
    public final static int CYAN_700 = 0x0097A7;
    public final static int CYAN_800 = 0x00838F;
    public final static int CYAN_900 = 0x006064;
    public final static int CYAN_A100 = 0x84FFFF;
    public final static int CYAN_A200 = 0x18FFFF;
    public final static int CYAN_A400 = 0x00E5FF;
    public final static int CYAN_A700 = 0x00B8D4;

    public final static int TEAL_50 = 0xE0F2F1;
    public final static int TEAL_100 = 0xB2DFDB;
    public final static int TEAL_200 = 0x80CBC4;
    public final static int TEAL_300 = 0x4DB6AC;
    public final static int TEAL_400 = 0x26A69A;
    public final static int TEAL_500 = 0x009688;
    public final static int TEAL_600 = 0x00897B;
    public final static int TEAL_700 = 0x00796B;
    public final static int TEAL_800 = 0x00695C;
    public final static int TEAL_900 = 0x004D40;
    public final static int TEAL_A100 = 0xA7FFEB;
    public final static int TEAL_A200 = 0x64FFDA;
    public final static int TEAL_A400 = 0x1DE9B6;
    public final static int TEAL_A700 = 0x00BFA5;

    public final static int GREEN_50 = 0xE8F5E9;
    public final static int GREEN_100 = 0xC8E6C9;
    public final static int GREEN_200 = 0xA5D6A7;
    public final static int GREEN_300 = 0x81C784;
    public final static int GREEN_400 = 0x66BB6A;
    public final static int GREEN_500 = 0x4CAF50;
    public final static int GREEN_600 = 0x43A047;
    public final static int GREEN_700 = 0x388E3C;
    public final static int GREEN_800 = 0x2E7D32;
    public final static int GREEN_900 = 0x1B5E20;
    public final static int GREEN_A100 = 0xB9F6CA;
    public final static int GREEN_A200 = 0x69F0AE;
    public final static int GREEN_A400 = 0x00E676;
    public final static int GREEN_A700 = 0x00C853;

    public final static int LIGHT_GREEN_50 = 0xF1F8E9;
    public final static int LIGHT_GREEN_100 = 0xDCEDC8;
    public final static int LIGHT_GREEN_200 = 0xC5E1A5;
    public final static int LIGHT_GREEN_300 = 0xAED581;
    public final static int LIGHT_GREEN_400 = 0x9CCC65;
    public final static int LIGHT_GREEN_500 = 0x8BC34A;
    public final static int LIGHT_GREEN_600 = 0x7CB342;
    public final static int LIGHT_GREEN_700 = 0x689F38;
    public final static int LIGHT_GREEN_800 = 0x558B2F;
    public final static int LIGHT_GREEN_900 = 0x33691E;
    public final static int LIGHT_GREEN_A100 = 0xCCFF90;
    public final static int LIGHT_GREEN_A200 = 0xB2FF59;
    public final static int LIGHT_GREEN_A400 = 0x76FF03;
    public final static int LIGHT_GREEN_A700 = 0x64DD17;

    public final static int LIME_50 = 0xF9FBE7;
    public final static int LIME_100 = 0xF0F4C3;
    public final static int LIME_200 = 0xE6EE9C;
    public final static int LIME_300 = 0xDCE775;
    public final static int LIME_400 = 0xD4E157;
    public final static int LIME_500 = 0xCDDC39;
    public final static int LIME_600 = 0xC0CA33;
    public final static int LIME_700 = 0xAFB42B;
    public final static int LIME_800 = 0x9E9D24;
    public final static int LIME_900 = 0x827717;
    public final static int LIME_A100 = 0xF4FF81;
    public final static int LIME_A200 = 0xEEFF41;
    public final static int LIME_A400 = 0xC6FF00;
    public final static int LIME_A700 = 0xAEEA00;

    public final static int YELLOW_50 = 0xFFFDE7;
    public final static int YELLOW_100 = 0xFFF9C4;
    public final static int YELLOW_200 = 0xFFF59D;
    public final static int YELLOW_300 = 0xFFF176;
    public final static int YELLOW_400 = 0xFFEE58;
    public final static int YELLOW_500 = 0xFFEB3B;
    public final static int YELLOW_600 = 0xFDD835;
    public final static int YELLOW_700 = 0xFBC02D;
    public final static int YELLOW_800 = 0xF9A825;
    public final static int YELLOW_900 = 0xF57F17;
    public final static int YELLOW_A100 = 0xFFFF8D;
    public final static int YELLOW_A200 = 0xFFFF00;
    public final static int YELLOW_A400 = 0xFFEA00;
    public final static int YELLOW_A700 = 0xFFD600;

    public final static int AMBER_50 = 0xFFF8E1;
    public final static int AMBER_100 = 0xFFECB3;
    public final static int AMBER_200 = 0xFFE082;
    public final static int AMBER_300 = 0xFFD54F;
    public final static int AMBER_400 = 0xFFCA28;
    public final static int AMBER_500 = 0xFFC107;
    public final static int AMBER_600 = 0xFFB300;
    public final static int AMBER_700 = 0xFFA000;
    public final static int AMBER_800 = 0xFF8F00;
    public final static int AMBER_900 = 0xFF6F00;
    public final static int AMBER_A100 = 0xFFE57F;
    public final static int AMBER_A200 = 0xFFD740;
    public final static int AMBER_A400 = 0xFFC400;
    public final static int AMBER_A700 = 0xFFAB00;

    public final static int ORANGE_50 = 0xFFF3E0;
    public final static int ORANGE_100 = 0xFFE0B2;
    public final static int ORANGE_200 = 0xFFCC80;
    public final static int ORANGE_300 = 0xFFB74D;
    public final static int ORANGE_400 = 0xFFA726;
    public final static int ORANGE_500 = 0xFF9800;
    public final static int ORANGE_600 = 0xFB8C00;
    public final static int ORANGE_700 = 0xF57C00;
    public final static int ORANGE_800 = 0xEF6C00;
    public final static int ORANGE_900 = 0xE65100;
    public final static int ORANGE_A100 = 0xFFD180;
    public final static int ORANGE_A200 = 0xFFAB40;
    public final static int ORANGE_A400 = 0xFF9100;
    public final static int ORANGE_A700 = 0xFF6D00;

    public final static int DEEP_ORANGE_50 = 0xFBE9E7;
    public final static int DEEP_ORANGE_100 = 0xFFCCBC;
    public final static int DEEP_ORANGE_200 = 0xFFAB91;
    public final static int DEEP_ORANGE_300 = 0xFF8A65;
    public final static int DEEP_ORANGE_400 = 0xFF7043;
    public final static int DEEP_ORANGE_500 = 0xFF5722;
    public final static int DEEP_ORANGE_600 = 0xF4511E;
    public final static int DEEP_ORANGE_700 = 0xE64A19;
    public final static int DEEP_ORANGE_800 = 0xD84315;
    public final static int DEEP_ORANGE_900 = 0xBF360C;
    public final static int DEEP_ORANGE_A100 = 0xFF9E80;
    public final static int DEEP_ORANGE_A200 = 0xFF6E40;
    public final static int DEEP_ORANGE_A400 = 0xFF3D00;
    public final static int DEEP_ORANGE_A700 = 0xDD2C00;

    public final static int BROWN_50 = 0xEFEBE9;
    public final static int BROWN_100 = 0xD7CCC8;
    public final static int BROWN_200 = 0xBCAAA4;
    public final static int BROWN_300 = 0xA1887F;
    public final static int BROWN_400 = 0x8D6E63;
    public final static int BROWN_500 = 0x795548;
    public final static int BROWN_600 = 0x6D4C41;
    public final static int BROWN_700 = 0x5D4037;
    public final static int BROWN_800 = 0x4E342E;
    public final static int BROWN_900 = 0x3E2723;

    public final static int GREY_50 = 0xFAFAFA;
    public final static int GREY_100 = 0xF5F5F5;
    public final static int GREY_200 = 0xEEEEEE;
    public final static int GREY_300 = 0xE0E0E0;
    public final static int GREY_400 = 0xBDBDBD;
    public final static int GREY_500 = 0x9E9E9E;
    public final static int GREY_600 = 0x757575;
    public final static int GREY_700 = 0x616161;
    public final static int GREY_800 = 0x424242;
    public final static int GREY_900 = 0x212121;

    public final static int BLUE_GREY_50 = 0xECEFF1;
    public final static int BLUE_GREY_100 = 0xCFD8DC;
    public final static int BLUE_GREY_200 = 0xB0BEC5;
    public final static int BLUE_GREY_300 = 0x90A4AE;
    public final static int BLUE_GREY_400 = 0x78909C;
    public final static int BLUE_GREY_500 = 0x607D8B;
    public final static int BLUE_GREY_600 = 0x546E7A;
    public final static int BLUE_GREY_700 = 0x455A64;
    public final static int BLUE_GREY_800 = 0x37474F;
    public final static int BLUE_GREY_900 = 0x263238;

    public final static int BLACK = 0x000000;
    public final static int WHITE = 0xFFFFFF;

}
/*
<div id=i contenteditable=true></div>

var i = document.getElementById("i");

var input = i.innerText;

var a = input.split("\n");
var c = "BLUE";
var re = "";
var tmp;
for(var i =0;i<a.length;i++) {
	tmp = a[i].split("#");
	re += "public final static int ";
	re += c;
	re += "_";
	re += tmp[0];
	re += " = 0x";
	re += tmp[1];
	re += ";\n";
}
*/
