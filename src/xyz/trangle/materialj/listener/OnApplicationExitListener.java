package xyz.trangle.materialj.listener;

/**
 * Created by Trangle on 2017/7/13.
 */
public interface OnApplicationExitListener {
    void onApplicationExit();
}
