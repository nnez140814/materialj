/* Copyright (C) 2017 MaterialJ Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.trangle.materialj.content;

/**
 * Created by Trangle on 17-7-12.
 * <p>
 * Global information of an application.
 */
public abstract class Context {

    //********** Fields, Getters & Setters **********

    /**
     * Specify the name of this app, which will be shown in
     * TaskManager on Windows.
     */
    private String mAppName;

    public String getAppName() {
        return mAppName;
    }

    public void setAppName(String appName) {
        mAppName = appName;
    }

    /**
     * Specify the description of this app, which will
     * be shown in TaskManager on Windows.
     */
    private String mAppDescription;

    public String getAppDescription() {
        return mAppDescription;
    }

    public void setAppDescription(String appDescription) {
        this.mAppDescription = appDescription;
    }

    /**
     * Will draw layout bounds if true.
     * <p>
     * Red line with blue corner: bounds
     * Pink rectangle: margin space
     */
    protected boolean mDrawLayoutBoundsEnabled = false;

    public boolean isDrawLayoutBoundsEnabled() {
        return mDrawLayoutBoundsEnabled;
    }
}
