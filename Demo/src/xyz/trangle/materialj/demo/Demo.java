package xyz.trangle.materialj.demo;/* Copyright (C) 2017 MaterialJ Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import xyz.trangle.materialj.app.Application;
import xyz.trangle.materialj.view.Window;

/**
 * Created by Trangle on 17-7-12.
 * <p>
 * A demo to MaterialJ
 */
public class Demo {
    Demo(String[] args) {
        Application app = new Application(args);

        int wid = app.createWindow("Hello MaterialJ");
        Window window = app.getWindow(wid);
        window.setVisible(true);

//        System.out.println(this.getClass().getResource("/res/test.file"));
    }

    public static void main(String[] args) {
        new Demo(args);
    }
}
